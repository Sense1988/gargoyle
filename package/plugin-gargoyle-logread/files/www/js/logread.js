/*
 * This program is copyright © 2012-2013 Cezary Jackiewicz and is distributed under the terms of the GNU GPL 
 * version 2.0 with a special clarification/exception that permits adapting the program to 
 * configure proprietary "back end" software provided that all modifications to the web interface
 * itself remain covered by the GPL. 
 * See http://gargoyle-router.com/faq.html#qfoss for more information
 */function resetData(){var e=[];e.push("logread"),setControlsEnabled(!1,!0,sylS.Load);var t=getParameterDefinition("commands",e.join("\n"))+"&"+getParameterDefinition("hash",document.cookie.replace(/^.*hash=/,"").replace(/[\t ;]+.*$/,"")),n=function(e){e.readyState==4&&(document.getElementById("output").value=e.responseText,setControlsEnabled(!0))};runAjax("POST","utility/run_commands.sh",t,n)}var sylS=new Object;